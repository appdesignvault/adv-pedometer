//
//  LineView.h
//  Pedometer++
//
//  Created by Gabriel Theodoropoulos on 14/3/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineView : UIView

@property (nonatomic, strong) UIColor *lineColor;

@property (nonatomic) BOOL horizontalLine;

-(id)initWithFrame:(CGRect)frame andLineColor:(UIColor *)color isHorizontalLine:(BOOL)isHorizontal;

@end

//
//  WalkingInfo.h
//  Pedometer++
//
//  Created by Gabriel Theodoropoulos on 14/3/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WalkingInfo : NSObject

@property (nonatomic) NSInteger steps;

@property (nonatomic) float distance;


-(id)initWithSteps:(int)steps andDistance:(float)distance;

@end

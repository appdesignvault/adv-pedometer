//
//  WalkingInfo.m
//  Pedometer++
//
//  Created by Gabriel Theodoropoulos on 14/3/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import "WalkingInfo.h"

@implementation WalkingInfo

-(id)initWithSteps:(int)steps andDistance:(float)distance{
    self = [super init];
    if (self) {
        self.steps = steps;
        self.distance = distance;
    }
    return self;
}

@end

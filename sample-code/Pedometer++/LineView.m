//
//  LineView.m
//  Pedometer++
//
//  Created by Gabriel Theodoropoulos on 14/3/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import "LineView.h"

@implementation LineView

-(id)initWithFrame:(CGRect)frame andLineColor:(UIColor *)color isHorizontalLine:(BOOL)isHorizontal{
    self = [super initWithFrame:frame];
    if (self) {
        self.lineColor = color;
        self.horizontalLine = isHorizontal;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
    CGContextSetLineWidth(context, 5.0);
    CGContextMoveToPoint(context, 0.0, 0.0);
    if (self.horizontalLine) {
        CGContextAddLineToPoint(context, self.frame.size.width, 0.0);
    }
    else{
        CGContextAddLineToPoint(context, 0.0, self.frame.size.height);
    }
    
    CGContextStrokePath(context);
}

@end

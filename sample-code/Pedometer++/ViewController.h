//
//  ViewController.h
//  Pedometer++
//
//  Created by Gabriel Theodoropoulos on 14/3/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *lblDay1;

@property (nonatomic, weak) IBOutlet UILabel *lblDay2;

@property (nonatomic, weak) IBOutlet UILabel *lblDay3;

@property (nonatomic, weak) IBOutlet UILabel *lblDay4;

@property (nonatomic, weak) IBOutlet UILabel *lblDay5;

@property (nonatomic, weak) IBOutlet UILabel *lblDay6;

@property (nonatomic, weak) IBOutlet UILabel *lblDay7;

@property (nonatomic, weak) IBOutlet UILabel *lblTotalsReport;

@property (nonatomic, weak) IBOutlet UILabel *lblSteps;

@property (nonatomic, weak) IBOutlet UILabel *lblDistance;

@end

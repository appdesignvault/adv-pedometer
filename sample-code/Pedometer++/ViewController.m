//
//  ViewController.m
//  Pedometer++
//
//  Created by Gabriel Theodoropoulos on 14/3/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import "ViewController.h"
#import "WalkingInfo.h"
#import "LineView.h"
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController () <CLLocationManagerDelegate>

// This array is used to store the step and distance data as WalkingInfo objects.
@property (nonatomic, strong) NSMutableArray *arrWalkingData;

// This array is used to store the dates that match to the presented seven days.
@property (nonatomic, strong) NSMutableArray *arrPresentedDates;

// The bottom Y-point value of the bar views.
@property (nonatomic) CGFloat barBottomYPoint;

// The top most Y-point value that a bar view can reach.
@property (nonatomic) CGFloat topMostYPoint;

// The desired width of the step and distance labels existing on the bar views.
@property (nonatomic) CGFloat desiredLabelWidth;

// The desired height of the step and distance labels existing on the bar views.
@property (nonatomic) CGFloat desiredLabelHeight;

// The number of steps that equal to a screen point.
@property (nonatomic) float stepsPerPoint;

// The total number of steps in a week
@property (nonatomic) NSInteger totalSteps;

// The total distance traveled in a week.
@property (nonatomic) float totalDistance;

// The percentage of the completed goals.
@property (nonatomic) float totalCompletedGoal;

// A flag indicating if the setupBarViews method is called for first time or not, used to specify if animation should
// occur or not for displaying the bar views and their respective step and distance labels.
@property (nonatomic) BOOL isSettingUpBarViewsForFirstTime;

// A NSOperationQueue object for executing step counting tasks.
@property (nonatomic, strong) NSOperationQueue *operationQueue;

// A step counter object.
@property (nonatomic, strong) CMStepCounter *stepCounter;

// A CLLocationManager object for managing locations.
@property (nonatomic, strong) CLLocationManager *locationManager;

// A CLLocation object representing the last location of the device.
@property (nonatomic, strong) CLLocation *lastLocation;


-(void)makeInitializations;
-(NSString *)getFormattedStringFromNumber:(NSInteger)number;
-(BOOL)checkIfRequireGraphicalAdjustments;
-(void)setupBarViews;
-(void)calculatePresentedDates;
-(void)setDayNames;
-(void)calculateTotals;
-(void)updateTotalsReport;
-(void)createLines;
-(void)performStepCounterTasks;
-(void)updateBar;
-(void)fixMinimumHeightForBarAtIndex:(int)index;
-(void)setupDistanceTracking;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Make all necessary initializations.
    [self makeInitializations];
    
    // Change the steps per point if needed, in case a bar view is too tall to fit on the given available space.
    [self checkIfRequireGraphicalAdjustments];
    
    // Calculate all totals that will be displayed to the respective label.
    [self calculateTotals];
    
    // Once the view has appeared, update the totals label with the values that have already been calculated.
    [self updateTotalsReport];
    
    // Calculate all the dates matching to the past seven days.
    [self calculatePresentedDates];
    
    // Setup all bar views and their respective step and distance labels.
    //[self setupBarViews];
    
    // Set the day names to the appropriate labels.
    [self setDayNames];
    
    // Create all lines.
    [self createLines];
    
    // Perform all tasks required to start counting steps.
    [self performStepCounterTasks];
    
    // Perform all tasks for tracking and calculating the travelled distance.
    [self setupDistanceTracking];
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private method implementation

-(void)makeInitializations{
    // Initialize the arrWalkingData array that keeps the walking data and fill it with some default values.
    self.arrWalkingData = [[NSMutableArray alloc] init];
    for (int i=0; i<7; i++) {
        [self.arrWalkingData addObject:[[WalkingInfo alloc] initWithSteps:0 andDistance:0.0]];
    }
    
    // Initialize the arrPresentedDates array. This one will keep all the dates matching to the shown days.
    self.arrPresentedDates = [[NSMutableArray alloc] init];
    
    // Calculate the bottom y-point of each bar.
    self.barBottomYPoint = self.lblDay1.frame.origin.y - 8.0;
    
    // Specify the desired width and height values for the distance and step labels that will appear on top and
    // above the bar views.
    self.desiredLabelWidth = self.lblDay1.frame.size.width;
    self.desiredLabelHeight = self.lblDay1.frame.size.height;
    
    // Calculate the top most Y point value that a bar view is allows to reach.
    self.topMostYPoint = self.view.bounds.size.height/2 + self.desiredLabelHeight + 10.0;
    
    // Set an initial value for the stepsPerPoints property. This value will be changed and decreased if
    // a bar view becomes too tall and the smaller bars need to be redraw in order to to keep the correct ratio
    // between the steps and the screen points.
    self.stepsPerPoint = 20.0;
    
    // This flag is needed in order to distinguish the first one from the next calls of the setupBarViews method,
    // where the initial operation should be performed.
    self.isSettingUpBarViewsForFirstTime = YES;
    
    // Instantiate the operation queue property.
    self.operationQueue = [NSOperationQueue new];
}


-(NSString *)getFormattedStringFromNumber:(NSInteger)number{
    // Initialize a number formatter object that will be used to format an integer number to the form: 1,234
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    // Indicate that we want the number to use a group separator.
    numberFormatter.usesGroupingSeparator = YES;
    // Use the current locale to make sure that the number will be properly presented in all metric systems.
    numberFormatter.locale = [NSLocale currentLocale];
    // Specify the desired number style.
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    // Return a string created by the number of the parameter and formatted according to the settings above.
    return [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
}


-(BOOL)checkIfRequireGraphicalAdjustments{
    // Find the max step value among all step data.
    WalkingInfo *info = [self.arrWalkingData objectAtIndex:0];
    NSInteger maxSteps = info.steps;
    for (int i=1; i<7; i++) {
        info = [self.arrWalkingData objectAtIndex:i];
        if (info.steps > maxSteps) {
            maxSteps = info.steps;
        }
    }
    
    // Calculate the max possible bar view height.
    CGFloat maxBarHeight = (maxSteps / self.stepsPerPoint) / [UIScreen mainScreen].scale;
    
    // Define the y-origin point value of the tallest bar view.
    CGFloat maxBarYPoint = self.barBottomYPoint - maxBarHeight;
    
    // If the maxBarYPoint value is lower than the top most allowed y-point value, then the bar view is too tall, so
    // it must take the maximum allowed height and re-calculate the stepsPerPoint property value.
    if (maxBarYPoint < self.topMostYPoint) {
        maxBarHeight = self.barBottomYPoint - self.topMostYPoint - 20.0;
        self.stepsPerPoint = (maxSteps / maxBarHeight) / [UIScreen mainScreen].scale;
        
        return YES;
    }
    else{
        return NO;
    }
}


-(void)setupBarViews{
    // Initialize an array that will be used to keep the y-point values of the bar views.
    NSMutableArray *yPointsArray = [[NSMutableArray alloc] init];
    
    if (!self.isSettingUpBarViewsForFirstTime) {
        // In case that this is not the first time that this method is called, so the isSettingUpBarViewsForFirstTime == NO,
        // and all bar-related views should be updated, they must firstly be removed from the view
        // as they will be added to it again.
        for (int i=0; i<7; i++) {
            [[self.view viewWithTag:(i + 1 ) * 10] removeFromSuperview];
            [[self.view viewWithTag:(i + 1 ) * 100] removeFromSuperview];
            [[self.view viewWithTag:(i + 1 ) * 1000] removeFromSuperview];
        }
    }
    
    for (int i=0; i<7; i++) {
        // Specify the x-point for each bar and for their respective labels.
        // It is the same to each day label's x origin value.
        CGFloat xPoint = [self.view viewWithTag:i+1].frame.origin.x;
        
        // Calculate the y origin value for each bar view taking under account the stepsPerPoint property and the
        // screen scale (retina display).
        WalkingInfo *info = [self.arrWalkingData objectAtIndex:i];
        CGFloat yPoint = self.barBottomYPoint - (info.steps / self.stepsPerPoint) / [UIScreen mainScreen].scale;
        
        // Add each calculated y-point value to the array. It will be needed later.
        [yPointsArray addObject:[NSNumber numberWithFloat:yPoint]];
        
        
        // Create and setup the distance labels.
        UILabel *distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPoint, self.barBottomYPoint - self.desiredLabelHeight, self.desiredLabelWidth, self.desiredLabelHeight)];
        distanceLabel.text = [NSString stringWithFormat:@"%.1f km", info.distance];
        distanceLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:12.0];
        distanceLabel.textAlignment = NSTextAlignmentCenter;
        distanceLabel.textColor = [UIColor whiteColor];
        distanceLabel.tag = (i + 1) * 100;  // 100, 200, 300, ...
        
        // If the method is called for first time then set the alpha value to 0, otherwise to 1. This is done
        // for animation reasons.
        distanceLabel.alpha = (self.isSettingUpBarViewsForFirstTime) ? 0.0 : 1.0;
        
        
        // Create and setup the steps labels.
        UILabel *stepsLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPoint, yPoint - self.lblDay1.frame.size.height, self.lblDay1.frame.size.width, self.lblDay1.frame.size.height)];
        stepsLabel.text = [NSString stringWithFormat:@"%@", [self getFormattedStringFromNumber:info.steps]];
        stepsLabel.font = [UIFont fontWithName:@"Avenir-Heavy" size:12.0];
        stepsLabel.textAlignment = NSTextAlignmentCenter;
        stepsLabel.textColor = [UIColor colorWithRed:1.0 green:0.2 blue:0.2 alpha:1.0];
        stepsLabel.tag = (i + 1) * 1000;    // 1000, 2000, 3000, ...
        
        // If the method is called for first time then set the alpha value to 0, otherwise to 1. This is done
        // for animation reasons.
        stepsLabel.alpha = (self.isSettingUpBarViewsForFirstTime) ? 0.0 : 1.0;
        
        
        // Create the bar views.
        // Create two frames and if this is the first time this method is called, initialize each bar using the initialRect
        // frame, otherwise set the finalRect frame. The initial frame is used to perform the animation upon the first
        // display of the bar views.
        CGRect initialRect = CGRectMake(xPoint, self.barBottomYPoint, self.desiredLabelWidth, 0.0);
        
        CGRect finalRect = CGRectMake(xPoint, [[yPointsArray objectAtIndex:i] floatValue], self.desiredLabelWidth, (info.steps / self.stepsPerPoint) / [UIScreen mainScreen].scale);
        
        UIView *barView = (self.isSettingUpBarViewsForFirstTime) ? [[UIView alloc] initWithFrame:initialRect] : [[UIView alloc] initWithFrame:finalRect];
        
        // Set the background color of each bar view.
        barView.backgroundColor = [UIColor colorWithRed:1.0 green:0.2 blue:0.2 alpha:1.0];
        
        // Set the tag values for each bar view.
        barView.tag = (i + 1) * 10; // The bars' tag values will be 10, 20, 30, ..., and so on.
        
        if (!self.isSettingUpBarViewsForFirstTime) {
            // After each bar view setup, make sure that it has the minimum required height.
            [self fixMinimumHeightForBarAtIndex:i];
        }
        
        // Add all previous subviews to the main view.
        [self.view addSubview:barView];
        [self.view addSubview:distanceLabel];
        [self.view addSubview:stepsLabel];
        
    }
    
    // If the method is called for first time then display the bar views and their respective step and distance labels
    // using animations.
    if (self.isSettingUpBarViewsForFirstTime) {
        [UIView animateWithDuration:0.5 animations:^{
            for (int i=0; i<7; i++) {
                // Set the final frame to each bar view.
                WalkingInfo *info = [self.arrWalkingData objectAtIndex:i];
                UIView *barView = [self.view viewWithTag:(i + 1) * 10];
                barView.frame = CGRectMake(barView.frame.origin.x,
                                           [[yPointsArray objectAtIndex:i] floatValue],
                                           self.desiredLabelWidth,
                                           (info.steps / self.stepsPerPoint) / [UIScreen mainScreen].scale);
                
                // Make sure that each bar view meets the minimum allowed height value.
                [self fixMinimumHeightForBarAtIndex:i];
            }
            
        } completion:^(BOOL finished) {
            // When the display animation of the bar views is over, then using a new one show the step and distance labels.
            if (finished) {
                for (int i=0; i<7; i++) {
                    [UIView animateWithDuration:0.25 animations:^{
                        [self.view viewWithTag:(i + 1) * 100].alpha = 1.0;
                        [self.view viewWithTag:(i + 1) * 1000].alpha = 1.0;
                    }];
                }
            }
        }];
    }
    
    // Change the flag's value.
    if (self.isSettingUpBarViewsForFirstTime) {
        self.isSettingUpBarViewsForFirstTime = NO;
    }
}


-(void)calculatePresentedDates{
    // Get today's date.
    NSDate *today = [NSDate date];
    
    // Instantiate a NSCaledar object that will help to calculate previous dates.
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    // Initialize a NSDateComponents object that in combination with the today date and the calendar object
    // will give the desired previous date.
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    
    // Using a loop calculate all previous seven dates (including today).
    for (int i=0; i<7; i++) {
        // Specify for how many days ago a date must be calculated.
        [dateComponents setDay:i-6];
        
        // Create a new NSDate object by adding the negative days set in the dateComponents object
        // to the today date.
        NSDate *date = [calendar dateByAddingComponents:dateComponents
                                                 toDate:today
                                                options:0];
        
        // Add the date to the array.
        [self.arrPresentedDates addObject:date];
    }
}


-(void)setDayNames{
    // Initialize a NSDateFormatter object to specify the desired date format.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];   // It returns day names such as Mond, Tues, ...
    
    // Do all work in a -for- loop so as all day labels to be accessed.
    for (int i=0; i<7; i++) {
        // Get each day label using its tag.
        UILabel *dayLabel = (UILabel *)[self.view viewWithTag:i + 1];
        
        // Specify the day name string based on the date existing to the current index of the arrPresentedDates array.
        NSString *dayName = [dateFormatter stringFromDate:[self.arrPresentedDates objectAtIndex:i]];
        
        // As text to the label add the first three letters of the day name.
        dayLabel.text = [dayName substringToIndex:3];
    }
}


-(void)calculateTotals{
    // Calculate the total steps made and distance travelled within a week.
    self.totalSteps = 0;
    self.totalDistance = 0.0;
    
    for (NSInteger i=0; i<7; i++) {
        WalkingInfo *info = [self.arrWalkingData objectAtIndex:i];
        
        self.totalSteps += info.steps;
        self.totalDistance += info.distance;
    }
}


-(void)updateTotalsReport{
    // Create the string that will be set as text to the totals label.
    // The upper dot symbol is represented with the 0x00b7 code.
    NSString *reportText = [NSString stringWithFormat:@"%@ %c %.1f km %c %.0f%% of Goal", [self getFormattedStringFromNumber:self.totalSteps], 0x00b7, self.totalDistance, 0x00b7, self.totalCompletedGoal];
    
    self.lblTotalsReport.text = reportText;
}


-(void)createLines{
    // The green horizontal line to the center.
    LineView *greenLine = [[LineView alloc] initWithFrame:CGRectMake(0.0, self.view.bounds.size.height/2, self.view.bounds.size.width, 1.5) andLineColor:[UIColor colorWithRed:0.58 green:0.89 blue:0.6 alpha:1.0] isHorizontalLine:YES];
    [self.view addSubview:greenLine];
    
    // The lines next to the totals report label.
    // Left horizontal line.
    CGRect frameForLeftHorizontalLine = CGRectMake(self.lblDay1.frame.origin.x,
                                                   self.lblTotalsReport.center.y,
                                                   self.lblTotalsReport.frame.origin.x - self.lblDay1.frame.origin.x,
                                                   1.5);
    LineView *leftHorizontalLine = [[LineView alloc] initWithFrame:frameForLeftHorizontalLine
                                                      andLineColor:[UIColor colorWithRed:1.0 green:0.2 blue:0.2 alpha:1.0]
                                                  isHorizontalLine:YES];
    [self.view addSubview:leftHorizontalLine];
    
    
    // Left vertical line.
    CGRect frameForLeftVerticalLine = CGRectMake(leftHorizontalLine.frame.origin.x,
                                                 leftHorizontalLine.frame.origin.y - 5.0,
                                                 1.5,
                                                 5.0);
    LineView *leftVerticalLine = [[LineView alloc] initWithFrame:frameForLeftVerticalLine
                                                    andLineColor:[UIColor colorWithRed:1.0 green:0.2 blue:0.2 alpha:1.0]
                                                isHorizontalLine:NO];
    [self.view addSubview:leftVerticalLine];
    
    
    // Right horizontal line.
    CGRect frameForRightHorizontalLine = CGRectMake(self.lblTotalsReport.frame.origin.x + self.lblTotalsReport.frame.size.width,
                                                    self.lblTotalsReport.center.y,
                                                    (self.lblDay7.frame.origin.x + self.lblDay7.frame.size.width) - (self.lblTotalsReport.frame.origin.x + self.lblTotalsReport.frame.size.width),
                                                    1.5);
    LineView *rightHorizontalLine = [[LineView alloc] initWithFrame:frameForRightHorizontalLine
                                                       andLineColor:[UIColor colorWithRed:1.0 green:0.2 blue:0.2 alpha:1.0]
                                                   isHorizontalLine:YES];
    [self.view addSubview:rightHorizontalLine];
    
    
    // Right vertical line.
    CGRect frameForRightVerticalLine = CGRectMake(self.lblDay7.frame.origin.x + self.lblDay7.frame.size.width,
                                                  frameForLeftVerticalLine.origin.y,
                                                  1.5,
                                                  6.5);
    LineView *rightVerticalLine = [[LineView alloc] initWithFrame:frameForRightVerticalLine
                                                     andLineColor:[UIColor colorWithRed:1.0 green:0.2 blue:0.2 alpha:1.0]
                                                 isHorizontalLine:NO];
    [self.view addSubview:rightVerticalLine];
}


-(void)performStepCounterTasks{
    // Check if the step counting feature is supported by the device.
    if ([CMStepCounter isStepCountingAvailable]) {
        // Initialize the stepCounter object.
        self.stepCounter = [[CMStepCounter alloc] init];
        
        // Get all historical step counting data from the device date by date.
        
        
        for (int i=0; i<7; i++) {
            
            NSDate *startDate = [self getStartOfDay:self.arrPresentedDates[i]];
            NSDate *endDate = [self getEndOfDay:self.arrPresentedDates[i]];
            
            [self.stepCounter queryStepCountStartingFrom:startDate
                                                      to:endDate
                                                 toQueue:self.operationQueue
                                             withHandler:^(NSInteger numberOfSteps, NSError *error) {
                                                 if (error == nil) {
                                                     // Store the historical step data for each date.
                                                     WalkingInfo *info = [self.arrWalkingData objectAtIndex:i];
                                                     info.steps = numberOfSteps;
                                                     [self.arrWalkingData replaceObjectAtIndex:i withObject:info];
                                                 }
                                             }];
            
        }
        
        // Update the UI.
        [self setupBarViews];
        
        // Start counting steps.
        [self.stepCounter startStepCountingUpdatesToQueue:self.operationQueue
                                                 updateOn:1
                                              withHandler:^(NSInteger numberOfSteps, NSDate *timestamp, NSError *error) {
                                                  if (error != nil) {
                                                      [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                          // Increase the number of total steps for the current day and store it back to the array again.
                                                          WalkingInfo *info = [self.arrWalkingData objectAtIndex:6];
                                                          info.steps++;
                                                          [self.arrWalkingData replaceObjectAtIndex:6 withObject:info];
                                                          
                                                          // Update the steps label.
                                                          self.lblSteps.text = [self getFormattedStringFromNumber:info.steps];
                                                          
                                                          // Update the last bar view, but first check if any graphical
                                                          // adjustments should be done first.
                                                          if ([self checkIfRequireGraphicalAdjustments]) {
                                                              // In that case, redraw all bars and their related views.
                                                              [self setupBarViews];
                                                          }
                                                          else{
                                                              // Otherwise, simply make the bar view taller.
                                                              [self updateBar];
                                                              
                                                              // Update the steps label above the bar.
                                                              UILabel *barSteplabel = (UILabel *)[self.view viewWithTag:7000];
                                                              barSteplabel.text = self.lblSteps.text;
                                                          }
                                                          
                                                          // Update totals.
                                                          [self calculateTotals];
                                                          [self updateTotalsReport];
                                                      }];
                                                  }
                                              }];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pedometer++"
                                                        message:@"Step counting is not supported by your device."
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Okay", nil];
        [alert show];
    }
}


-(void)updateBar{
    // Get the current walking info object from the last index of the array.
    WalkingInfo *info = [self.arrWalkingData lastObject];
    
    // Specifty the current bar view.
    UIView *barView = [self.view viewWithTag:70];
    
    // Calculate the extra height that should be added to the bar view.
    CGFloat heightToAdd = ((info.steps / self.stepsPerPoint) / [UIScreen mainScreen].scale) - barView.frame.size.height;
    
    // Update the frame.
    barView.frame = CGRectMake(barView.frame.origin.x,
                               barView.frame.origin.y - heightToAdd,
                               self.desiredLabelWidth,
                               barView.frame.size.height + heightToAdd);
    
    // Update the frame of the steps label above the bar view.
    [self.view viewWithTag:7000].frame = CGRectOffset([self.view viewWithTag:7000].frame, 0.0, -heightToAdd);
    
    // Fix the bar view's height if it's smaller than the allowed one. This is needed at the beginning of the counting
    // where steps == 0.
    [self fixMinimumHeightForBarAtIndex:6];
}


-(void)fixMinimumHeightForBarAtIndex:(int)index{
    // Get the bar view specified by the index parameter value using its tag.
    UIView *barView = [self.view viewWithTag:(index + 1) * 10];
    
    // Check if the bar's height is lower than the distance label's height.
    if (barView.frame.size.height < self.desiredLabelHeight) {
        UIView *distanceLabel = [self.view viewWithTag:(index + 1) * 100];
        
        // In that case make both heights equal and according to the distance label's height.
        barView.frame = distanceLabel.frame;
        
        // Make sure that the steps label doesn't overlap the distance label.
        UIView *stepsLabel = [self.view viewWithTag:(index + 1) * 1000];
        stepsLabel.frame = CGRectOffset(stepsLabel.frame, 0.0, (distanceLabel.frame.origin.y - stepsLabel.frame.size.height) - stepsLabel.frame.origin.y);
    }
}


-(void)setupDistanceTracking{
    // Initialize a CLLocationManager object, set it up and start updating location.
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
}


-(NSDate*)getStartOfDay:(NSDate*)date{
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *startComponent = [gregorian components: NSUIntegerMax fromDate:date];
    
    [startComponent setHour:0];
    [startComponent setMinute:0];
    [startComponent setSecond:0];
    
    return [gregorian dateFromComponents:startComponent];
}

-(NSDate*)getEndOfDay:(NSDate*)date{

    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *endComponent = [gregorian components: NSUIntegerMax fromDate:date];
    
    [endComponent setHour:23];
    [endComponent setMinute:59];
    [endComponent setSecond:0];
    
    return [gregorian dateFromComponents:endComponent];
}


#pragma mark - CLLocationManager Delegate method implementation

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    // Get the last location.
    CLLocation *currentLocation = [locations lastObject];
    
    // Initially set the location distance value to 0.
    CLLocationDistance locationDistance = 0.0;
    
    // If this is not the first location, then calculate the difference between the current and the last one.
    if (self.lastLocation != nil) {
        locationDistance = [currentLocation distanceFromLocation:self.lastLocation];
    }
    
    // Update the lastLocation property.
    self.lastLocation = currentLocation;
    
    // Update the WalkingInfo object in the last index of the array.
    WalkingInfo *info = [self.arrWalkingData lastObject];
    info.distance += locationDistance;
    [self.arrWalkingData replaceObjectAtIndex:6 withObject:info];
    
    // Update the text of the main distance label.
    self.lblDistance.text = [NSString stringWithFormat:@"%.1f", info.distance];
    
    // Update the text of distance label on the last bar view.
    UILabel *barDistanceLabel = (UILabel *)[self.view viewWithTag:700];
    barDistanceLabel.text = [NSString stringWithFormat:@"%.1f km", info.distance];
    
    // Note: The totals report label gets updated after a new step has beend performed,
    // so it's not necessary to do it here too.
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"%@", [error localizedDescription]);
}

@end
